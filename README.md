# etripwire

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with etripwire](#setup)
    * [What etripwire affects](#what-etripwire-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with etripwire](#beginning-with-etripwire)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Overview

This module is meant to deployt Enterprise Tripwire with the 
installer zip file.  It will download the file url, unzip it, and 
run the installer command.

## Module Description

etripwire puppet module is designed to use the puppi::netinstall resource 
provided in example42/puppi to run some exec commands.  The 
exec commands download, unzip, and install the Enterprise Tripwire
client.

## Setup

Setup for module is minimal.  You will need to have the zip file with the installer 
(from Tripwire) on a downloadable url, and you will need to pass some parameters to 
the module to install.

### What etripwire affects

* This will deploy /usr/local/tripwire/{tripwire-installer,te} and use the configs
  in the zip file.
* Tripwire provides it's customers with zip files to update the agents.  This is the 
  zipfile used to do the intstall with this module.

### Beginning with etripwire

To use this module you will need a download location (URL) of the Tripwire updater zip,
the passphrase for the server connection, and the servername.

## Limitations

This has been tested on RHEL 5/6 and CentOS 5/6.

## Development

To assist with this module please contribute to https://gitlab.com/spidersddd/etripwire.git
and do a merge request.