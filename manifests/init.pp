# == Class: etripwire
#
# This class is for installing the Enterprise version of 
# Tripwire client.  It uses the Tripwire provided update package
# to install the client.  In the provided client is most of the
# configuration information.  You will need the passphrase, servername
# and source URL for the package.
#
# === Parameters
#
#  [*source_url*]
#  Source URL to download the update zip package.
#
# [*extracted_dir*]
# Directory that will be expanded out of the zip file.
#
# [*destination_dir*]
#  Directory the zip file should be extracted to.
#
# [*server_host*]
# hostname of the Enterprise Tripwire service is running on.
#
# [*passphrase*]
# passphrase to connect to the Enterprise Tripwire server.
#
# [*ensure*]
# State the service (twdeamon) should be in upon completion of install.
#
# [*package_mgr*]
# Is a package manager used to install the service.
# This set to false and no code to use a package manager has been written.
# 
# [*server_port*]
# Port the Tripwire service is running on at the server side.
#
# [*eula*]
# Whether or not to accept the eula.  Defaults to true to enable auto install.
#
# [*post_command_options*]
# Additional options for the tw_agent.bin command.  
#
# [*install_rtm*]
# Install Real Time Monitoring for Tripwire, default to false.
#
# === Examples
#
#  class { 'etripwire':
#    source_url  => 'http://somesiteon.com/internet/linux-updater-amd64.zip',
#    passphrase  => 'somepassphrase',
#    server_host => 'thehostname.site.net',
#  }
#
# === Authors
#
# Author Name <troy.klein@puppetlabs.com>
#
class etripwire (
  $source_url,
  $extracted_dir,
  $destination_dir,
  $server_host,
  $passphrase,
  $ensure = running,
  $package_mgr = false,
  $server_port = '9898',
  $eula = 'accept',
  $post_command_options = ' --IgnoreOSVersion --silent ',
  $install_rtm = false,
) {
  if !$package_mgr {
    validate_string($source_url, $extracted_dir, $destination_dir, $server_host)
    puppi::netinstall { 'etripwire':
      url                 => $source_url,
      extracted_dir       => $extracted_dir,
      destination_dir     => $destination_dir,
      postextract_command => "bash ${destination_dir}/${extract_dir}/te_agent.bin ${post_command_options} --eula ${eula} --server-host ${server_host} --server-port ${server_port} --passphrase ${passphrase} --install-rtm ${install_rtm}",
      notify              => Service['twdaemon'],
    }
    service { 'twdaemon':
      ensure     => $ensure,
      hasrestart => true,
      hasstatus  => true,
      require    => Puppi::Netinstall['etripwire'],
    }
  }
}
